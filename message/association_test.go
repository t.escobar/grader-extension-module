package message

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSerializeAndDeserializeAssociation(t *testing.T) {
	s := Association{
		EpochNano:  98765432123,
		StudentID:  "6131039421",
		PublicKey:  "PublicKey",
	}

	s.Deserialize(s.Serialize())
	assert.Equal(t, Association{
		EpochNano:  98765432123,
		StudentID:  "6131039421",
		PublicKey:  "PublicKey",
	}, s)
}

