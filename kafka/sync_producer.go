package kafka

import (
	"github.com/Shopify/sarama"
)

func ProduceAssociationMessage(message []byte)  {
	_, _, err := syncProducer.SendMessage(&sarama.ProducerMessage{
		Topic:     topic,
		Partition: AssociationPartition,
		Value:     sarama.ByteEncoder(message),
	})
	if err != nil {
		panic(err)
	}
}

func ProduceSubmissionMessage(message []byte)  {
	_, _, err := syncProducer.SendMessage(&sarama.ProducerMessage{
		Topic:     topic,
		Partition: SubmissionPartition,
		Value:     sarama.ByteEncoder(message),
	})
	if err != nil {
		panic(err)
	}
}
