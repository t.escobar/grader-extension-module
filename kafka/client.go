package kafka

import (
	"github.com/Shopify/sarama"
)

const topic = "event"

var syncProducer sarama.SyncProducer
var consumer sarama.Consumer

const AssociationPartition int32 = 0
const SubmissionPartition int32 = 1


func init() {
	config := sarama.NewConfig()
	config.Version = sarama.V2_0_0_0

	config.Producer.Partitioner = sarama.NewManualPartitioner
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Return.Successes = true

	client, err := sarama.NewClient([]string{"localhost:9093"}, config)
	if err != nil {
		panic(err)
	}

	syncProducer, err = sarama.NewSyncProducerFromClient(client)
	if err != nil {
		panic(err)
	}

	consumer, err = sarama.NewConsumerFromClient(client)
	if err != nil {
		panic(err)
	}
}
